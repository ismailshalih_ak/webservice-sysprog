var http = require('http');
const { parse } = require('querystring');

let state = "unknown"

http.createServer(onRequest).listen(process.env.PORT || 3000);

function onRequest(req, res) {
    if (req.method === 'POST') {
        let body = '';
        req.on('data', chunk => {
            body = parse(chunk.toString());
            if(body.command === 'True') {
                state = "True"
                res.write('{ code : 200 , status : True }')
                res.end()
            } else if(body.command === 'False') {
                state = "False"
                res.write('{ code : 200 , status : False }')
                res.end()
            } else res.end('{ code : 400 }')
        });
    }
    else if(req.method === 'GET') {
        res.writeHeader(200, { "Content-Type": "text/html" });
        res.end(`
            <!doctype html>
            <html>
                <head>
                    <meta http-equiv="refresh" content="10">
                </head>
                <body>
                    <h1>`+ state + `</h1>
                </body>
            </html>
        `);
    }
}